# The Devian Proposal

For long time the debian mainline distro has been reluctant to include substantial changes or keeping up to an ever-changing software scene

Stale projects, poorly formatted perl, systemd adoption without regard for init freedom, not-so-straightforward PR system, a poorly updated backports system and many other issues have been polluting both his repos and popularity

They've been following Red Hat philosophy while still being tainted by the long-established development philosophy devuan still follows, thus ending in a compromise that satisfies no-one

An alternative, a radical deviance is needed, hence why I propose this new distro, Devian.

The core functions of this distro will be three of the most promising systems and standards currently active, runit, ZFS and IPFS

## Code should be readable and straightforward

Editorconfig in internally developed projects will be *mandatory* as it establishes a commond standard for editors

Using formatters while developing will be highly encouraged, and code not uniformating to established formatters for a project will not be merged

Anyway this won't result in tiresome code uploads and reuploads, as automatic formatting hooks will be included in projects

## A roadmap

The distro will start as a testing equivalent, when the core code will reach a certain maturity it will propagate as stable, stable-backports and stable-extended*
Also, the general idea would be including the necessary material for building the distro inside the distro itself

*backported packages strictly including DEs and userspace apps, software whose devs strictly forbade or complained about including into stable (radare2 and IPFS) or packages that by not keeping up with upstream seriously impact user experience, like youtube-dl

### First things first

Fixing general problems in [debootstrap](https://salsa.debian.org/installer-team/debootstrap) and [sbuild](https://salsa.debian.org/debian/sbuild) the steps will include:

1. reformatting

2. integration of devuan templates by adding a new "flavour" variable identifying the distro (automatically derived from repos)

3. introducing build with podman support

4. introducing QEMU emulation support for cross-building on different architectures

Readapting [devuan-sdk](https://github.org/dyne/devuan-sdk) to the new building paradigm, and rewriting in python3

### Runit is nice runit is good

Write a python3 suite for converting systemd unit files into runit services (and optionally sysvinit services too)
Explore with runit devs the possibility of a kind of oneshot services which doesn't necessitate weird hacks

### ZFS

Including changes from [proxmox](https://git.proxmox.com) source code for including ZFS support

### IPFS

IPFS devs [complained](https://github.com/ipfs/go-ipfs/issues/5471) about the possibility of having incompatible older versions of go-ipfs daemon floating around in distros, hence why it'll be kept in testing in the first phase, then moved to extended

### Amprolla is nice, let's make it better

Amprolla allws creating partial repos and redirecting to parent repos for every package not present in current repo
This allows better flexibility, as your code floats atop of others mantainers code
It's currently used in devuan repos infrastructure
But it could be even better, what if it was tweaked to support go-ipfs daemon and redirect requests on ipfs.io for saving bandwith?

### New life on migrationtools

Long ago I've written a rewrite of [migrationtools](https://gitlab.com/future-ad-laboratory/migrationtools), the project is still incomplete but it adds support to migration between various formats and samba AD
The objective is turning this tool into a staple for being able to turn the distro into a full-fledged domain controller

## Future objectives considered

1. Integration of proxmox code regarding management of VMs

2. A system which updates code directly from upstream, packagizes and directly applies necessary changes

3. A distro's default and non-invasive method for sandboxing running applications by default (needless to say, it will need wayland and maybe OpenZFS too)

4. A full and updated upload of debian, devuan and devian repos to IPFS
